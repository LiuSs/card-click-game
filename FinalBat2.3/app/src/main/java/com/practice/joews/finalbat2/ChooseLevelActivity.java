package com.practice.joews.finalbat2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;
import java.util.Random;

public class ChooseLevelActivity extends AppCompatActivity implements View.OnClickListener {
    int level,chances = 3,score = 0,
    clickstate,//狀態變數，判斷牌面的一致性，根據狀態的可能性浮動
    clickloop,//回合變數，判斷牌面的需要被比對次數(根據第一張卡牌決定
    cardswich=0;//開關變數，0表示尚未啟用比對，1表示首張牌面被點選;
    Button bt_Stop;
    AlertDialog.Builder ad , ad_loss , ad_Fail,ad_Win,ad_Exit;
    TextView tv_Second,tv_chances,tv_score,level_passCount;
    private ProgressBar pgBar_time;
    private MediaPlayer mp;
    int i = 0,btn,clickCount = 0,levelCount = 0,passCount =0,lossCount = 0;
    int[] btnId = {R.id.Button,R.id.Button2,R.id.Button3,R.id.Button4,R.id.Button5,R.id.Button6,R.id.Button7,R.id.Button8,R.id.Button9};
    int[] btnmatchNum = new int[9];//按鈕編號，同時也是比對參照(0-8)
    int[] cmpCount = new int[4];//因為是3*3的遊戲，有4種可能
    int[] CmpCount_match = new int[cmpCount.length];//判斷牌面的點擊狀況
    int[] bgPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_level);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        setUp(); //建立按鈕實體 + 讀出Intent值 + 將按鈕背景設為joker + 建立按鈕監聽器 + 變數初始化
        AlertDiolog();//先彈出"準備好了嗎"之對話視窗
    }//Oncreate-------------------------------------------------------------------------------------

    public void Round(){
        clickCount = 0;
        CardInitialize();
        OnPreviewCardCountDown();
    }
    public void CardInitialize(){
        for(int i=0;i<cmpCount.length;i++){
            cmpCount[i]=0;
        }
        bgPath = new int[btnmatchNum.length];
        String str = "";

        //亂數生成
        Random rand = new Random();
        for(int i=0;i<btnmatchNum.length;i++){
        btnmatchNum[i] = rand.nextInt(4);//隨機範圍0 ~ 3 將八個按鈕各自分配0~3其中一個數字
            //若數字為0，則cmpCount[0]++ 以此類推分別統計0~3之亂數各自的按鈕有幾個
            cmpCount[btnmatchNum[i]]++;
            CmpCount_match[btnmatchNum[i]]++;
            str = str + btnmatchNum[i] ;
            bgPath[i] = getResources().getIdentifier("p"+btnmatchNum[i],"drawable",getPackageName());  //根據索引填入每個按鈕的代號，同時轉型成路徑
        }
        //將圖片放至按鈕
       for(int i = 0;i<9;i++){
            Button button = (Button)findViewById(btnId[i]);
            button.setBackgroundResource(bgPath[i]);
            button.setEnabled(true);
        }
    }
    //------------Intent回到主選單----------------------------
    public void goBackToMenu(){
        Intent intent = new Intent();
        intent.setClass(ChooseLevelActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //------------物件建構------------------------------------
    public void setUp(){
        //-------讀出關卡難度值-------------------------
        Bundle bundle = getIntent().getExtras();
        level = bundle.getInt("time");//取出關卡別
        tv_Second = (TextView)findViewById(R.id.tv_second);//秒數
        tv_chances = (TextView)findViewById(R.id.tv_Chances);//機會次數
            tv_chances.setText("機會數：" + chances);
        tv_score = (TextView)findViewById(R.id.tv_Score);
        level_passCount = (TextView)findViewById(R.id.textView_LevelpassCount);
            level_passCount.setText("過關數:" + passCount +" 失敗關數"+ lossCount);
        bt_Stop = (Button)findViewById(R.id.btn_Stop);
            bt_Stop.setOnClickListener(this);
        //-------Button---------------------------------
        for(int id : btnId){
            Button button = (Button)findViewById(id);
            button.setOnClickListener(this);
            button.setBackgroundResource(R.drawable.joker);
        }

        //初始化比對用變數
        for(int i=0;i<cmpCount.length;i++){
            cmpCount[i]=0;
        }
        //初始化分數
        score=0;
        chances=3;
        cardswich=0;
        //-------設定progressBar屬性--------------------
        pgBar_time = (ProgressBar) findViewById(R.id.pgBar_Time);
        setProgress();
    }//---------------------------------------------------------------

    //---圖片與變數重設
    public void objectInitialize(){
        for(int id : btnId){
            Button button = (Button)findViewById(id);
            button.setBackgroundResource(R.drawable.joker);
            button.setEnabled(true);
        }

        //初始化比對用變數
        /*for(int i=0;i<cmpCount.length;i++){
            cmpCount[i]=0;
        }*/
    }
    //------------Progress重置--------------------------------------------------
    public void setProgress() {
        pgBar_time.setMax(level + 1);
        pgBar_time.setProgress(level + 1);
    }
    //------------對話視窗-遊戲準備開始-----------------------------------------
    public void AlertDiolog() {
        ad = new AlertDialog.Builder(this);
        ad.setMessage("若點選 OK 則遊戲馬上開始！\n"); //設定視窗訊息
        ad.setCancelable(false);
        ad.setTitle("準備好了嗎"); //設定標題
        ad.setIcon(android.R.drawable.ic_menu_edit);

        //----------開始遊戲-----------------------------------------------
        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setMisic(); //撥放音樂
                playMisic();
                Round();
            }
        });

        //----------結束遊戲----------------------------------------------
        ad.setNegativeButton("結束遊戲", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });

        //----------回主選單-----------------------------------------------
        ad.setNeutralButton("回主選單", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goBackToMenu();
            }
        });
        ad.show();
    }
    //------------倒數計時------------------------------------------------------
    public void OnPreviewCardCountDown(){
        new CountDownTimer(3 * 1000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                //Toast.makeText(this,"遊戲開始!",Toast.LENGTH_SHORT).show();
                objectInitialize();
                OnCountDown();
            }
        }.start();
    }
    public void OnCountDown() {
        new CountDownTimer((level+1) * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                tv_Second.setText("剩餘時間:" + millisUntilFinished / 1000);
                pgBar_time.incrementProgressBy(-1);
                if(clickCount == 9){
                    score++;
                    tv_score.setText("目前分數：" + score + "分");
                }
            }

            @Override
            public void onFinish() {
                tv_Second.setText("剩餘時間:0");
                pgBar_time.incrementProgressBy(-1);
                if(clickCount == 9){
                    clickCount = 0;
                    passCount++;
                    level_passCount.setText("過關數:" + passCount +"    失敗關數"+ lossCount);
                    levelCount++;
                    if(levelCount == 5) {chances++;levelCount = 0;tv_chances.setText("機會數：" + chances);}
                    AlertWinDialog();
                }
                else {
                    lossCount++;
                    level_passCount.setText("過關數:" + passCount +"    失敗關數"+ lossCount);
                    AlertLossDialog();
                }
            }
        }.start();
    }

    public void AlertLossDialog(){ //判斷是否有機會
        //若機會用完
        if(chances == 1){
            ad_Fail = new AlertDialog.Builder(this);
            ad_Fail.setMessage("噢不...遊戲結束了:( \n" +
                    "您的總分為:"+score+"分\n" +
                    "過關數:"+passCount+"關\n" +
                    "失敗觀數:"+lossCount+"關\n"); //設定視窗訊息
            ad_Fail.setCancelable(false);
            ad_Fail.setTitle("Ohhhhhh!"); //設定標題
            ad_Fail.setIcon(android.R.drawable.ic_menu_edit);
            ad_Fail.setPositiveButton("返回", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    goBackToMenu();
                }
            });
            ad_Fail.show();
        }
        //若還有機會
        else {

            chances--;
            tv_chances.setText("機會數：" + chances);
            ad_loss = new AlertDialog.Builder(this);
            ad_loss.setMessage("時間到！損失一次機會\n"); //設定視窗訊息
            ad_loss.setCancelable(false);
            ad_loss.setTitle("Oooops!"); //設定標題
            ad_loss.setIcon(android.R.drawable.ic_menu_edit);
            //----------繼續遊戲-----------------------------------------------
            ad_loss.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setProgress();
                    Round();
                }
            });

            //----------回主選單-----------------------------------------------
            ad_loss.setNeutralButton("回主選單", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   AlertLeaveDialog();
                }
            });
            ad_loss.show();
        }
        i = 0;

    }

    public void AlertWinDialog(){ //過關!
        ad_Win = new AlertDialog.Builder(this);
        ad_Win.setMessage("恭喜過關！\n"); //設定視窗訊息
        ad_Win.setCancelable(false);
        ad_Win.setTitle("是否繼續下一關"); //設定標題
        ad_Win.setIcon(android.R.drawable.ic_menu_edit);

        //----------進入下一關-----------------------------------------------
        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setProgress();
                Round();
            }
        });

        //----------結束遊戲----------------------------------------------
        ad.setNegativeButton("結束遊戲", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });

        //----------回主選單-----------------------------------------------
        ad.setNeutralButton("回主選單", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertLeaveDialog();
            }
        });
        ad.show();
    }

    public void AlertLeaveDialog(){
        ad_Exit = new AlertDialog.Builder(this);
        ad_Exit.setMessage("確定離開遊戲嗎? \n" +
                "您的總分為:"+score+"分\n" +
                "過關數:"+passCount+"關\n" +
                "失敗觀數:"+lossCount+"關\n"); //設定視窗訊息
        ad_Exit.setCancelable(false);
        ad_Exit.setTitle("Are you sure?"); //設定標題
        ad_Exit.setIcon(android.R.drawable.ic_menu_edit);
        ad_Exit.setPositiveButton("回首頁", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goBackToMenu();
            }
        });

        ad_Exit.setNegativeButton("不，繼續遊戲", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setProgress();
                Round();
            }
        });
        ad_Exit.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Stop:
                goBackToMenu();
                break;
            case R.id.Button:
            case R.id.Button2:
            case R.id.Button3:
            case R.id.Button4:
            case R.id.Button5:
            case R.id.Button6:
            case R.id.Button7:
            case R.id.Button8:
            case R.id.Button9:
                for (int i = 0; i < 9; i++) {
                    if (v.getId() == btnId[i]) {
                        Button button = (Button) findViewById(btnId[i]);
                        button.setBackgroundResource(bgPath[i]);
                        btn = i;
                        button.setEnabled(false);
                        break;
                    }
                }
                if (cardswich == 0) {
                    cardswich = 1;
                    clickloop = cmpCount[btnmatchNum[btn]];
                    clickloop--;
                    clickCount++;
                    if (clickloop == 0) {
                        score++;
                        tv_score.setText("目前分數：" + score + "分");
                        cardswich = 0;//重設開關
                        break;
                    }else {
                        clickstate = btnmatchNum[btn]; //紀錄現在所點選的牌面號碼
                    }
                } else {
                    if (clickstate == btnmatchNum[btn]) { //假如後續選的牌與第一張選的牌一樣
                        clickloop--;
                        clickCount++;
                        if (clickloop == 0) {
                            score++;
                            tv_score.setText("目前分數：" + score + "分");
                            cardswich = 0;//重設開關
                            break;
                        }
                    } else {
                        clickCount = 0;
                        cardswich = 0; //猜錯必須重新啟動開關
                        score--;
                        tv_score.setText("目前分數：" + score + "分");
                        objectInitialize();
                    }
                }

                break;
        }//Switch
    }//OnClick



    public void setMisic(){
        if(level == 3) {
            mp = MediaPlayer.create(this, R.raw.yellowbee);
        }else if(level == 10){
            mp = MediaPlayer.create(this, R.raw.easy);
        }else if(level == 6){
            mp = MediaPlayer.create(this, R.raw.mediam);
        }else if(level == 5){
            mp = MediaPlayer.create(this, R.raw.hard);
        }else {
            mp = MediaPlayer.create(this, R.raw.yellowbee);
        }
        mp.setLooping(true);
    }
    public void playMisic(){
        mp.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mp.stop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mp.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mp.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
