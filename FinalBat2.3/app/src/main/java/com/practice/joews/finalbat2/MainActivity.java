package com.practice.joews.finalbat2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements
        RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    //----變數宣告----------------------------------------------------------------------------------
    Switch sw_music; //宣告Switch
    Boolean OnOff = true;  //是否開啟音樂

    ImageButton bt_difficulty,bt_exit,bt_how;
    Button bt_OK; //按鈕宣告
    RadioGroup rg_Level;  //RadioButton
    int time = 0; //關卡時間
    MediaPlayer mp; //音樂撥放

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

            ButtonView();//執行ButtonView方法
    }//Oncreate

    private void ButtonView() {
        // 建立按鈕實體
        bt_difficulty = (ImageButton)findViewById(R.id.button_ChooseDifficulty);
        bt_exit = (ImageButton)findViewById(R.id.button_Exit);
        bt_how = (ImageButton)findViewById(R.id.imageButton_How);
        sw_music = (Switch)findViewById(R.id.switch_MusicOnOff);
        //--------------------------------------------------------------
        // 註冊按鈕監聽器
        bt_difficulty.setOnClickListener(this);
        bt_exit.setOnClickListener(this);
        bt_how.setOnClickListener(this);
        sw_music.setOnCheckedChangeListener(this);
    }

    private void ButtonView_ChooseLevel(){

        rg_Level = (RadioGroup)findViewById(R.id.radioGroup);
        bt_OK = (Button)findViewById(R.id.button_OK);

        rg_Level.setOnCheckedChangeListener(this);
        bt_OK.setOnClickListener(this);
    }

    public void GotoChooseLevel(){
        mp.stop();
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putInt("time", time);
        intent.setClass(this, ChooseLevelActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);    //觸發換頁
        finish();   //結束本頁
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.button_ChooseDifficulty://點選按鈕則進入第二個頁面去選擇關卡
                setContentView(R.layout.choose_level);
                Toast.makeText(this,"選擇關卡",Toast.LENGTH_SHORT).show();
                ButtonView_ChooseLevel();
                break;
            case R.id.button_Exit://離開遊戲
                finish();
                break;
            case R.id.imageButton_How:
                AlertHowToPlay();
                break;
            case R.id.button_OK://選擇關卡
                if(time!=0) {
                    GotoChooseLevel();
                }else{
                    Toast.makeText(this,"您尚未選擇關卡",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void AlertHowToPlay() {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setMessage("1.進入遊戲後，會顯示答案牌，" +
                "請玩家記住所有同花色" +
                "的牌之相對位置。\n" +
                "2.三秒後會蓋牌，玩家須在倒數時間內" +
                "將同花色的牌組全部點開才會過關。\n" +
                "3.若點選到不同花色的牌組，則所有的牌面會重新蓋回去，並扣一分。\n" +
                "4.每點完一個牌組加一分，" +
                "剩餘的秒數會加至分數。\n"+
                "5.若時間內沒有完成關卡，則會扣一次機會。\n" +
                "6.每過五關則機會+1\n" +
                "祝您遊戲愉快!"); //設定視窗訊息
        ad.setCancelable(true);
        ad.setTitle("遊戲說明"); //設定標題
        ad.setIcon(android.R.drawable.ic_menu_edit);
        ad.show();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton radioButton = (RadioButton)group.findViewById(checkedId);
        switch (checkedId){
            case R.id.radioButton_easy:
                time = 6;
                break;
            case R.id.radioButton_forFun:
                time = 10;
                break;
            case R.id.radioButton_Medium:
                time = 5;
                break;
            case R.id.radioButton_hard:
                time = 4;
                break;
            case R.id.radioButton_extreme:
                time = 3;
                break;
        }
//        String Stime = Integer.toString(time);
//        Toast.makeText(this, Stime ,Toast.LENGTH_SHORT).show();
    }

    public void setMisic(){
        if(OnOff == true) {
            mp = MediaPlayer.create(this,R.raw.countingstars);
            mp.start();
        }else{
            mp.stop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mp = MediaPlayer.create(this,R.raw.countingstars);
        setMisic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mp.stop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mp.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Switch sw = (Switch)buttonView;
        if(isChecked){
            OnOff = true;
        }
        else
            OnOff = false;
        setMisic();
    }
}
